package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)
func main() {
	number := 40
	normal(number)
	workerPool(number)
}

// normal
func normal(number int) {
	start := time.Now()
	for i := 1; i <= number; i++ {
		fmt.Println(fib(i))
	}
	estimate := time.Since(start)
	logrus.Infof("end: %v", estimate)
}

func workerPool(number int) {
	start := time.Now()

	numberOfWorker := 8 // so lượng worker đc chạy cùng lúc
	jobs := make(chan int, number) // channel cho phép gửi vào goroutine các số để tính fibonacci
	results := make(chan int, number) // kết quả trả về đc lưu tạm vào đây, rồi đc lấy ra

	// giới hạn số lượng worker đc phép chạy cùng lúc, đảm bảo ko bị chết luồng
	for i := 0; i < numberOfWorker; i++ {
		go worker(jobs, results)
	}

	// gửi i vào
	for i := 1; i <= number; i++ {
		jobs <- i
	}
	close(jobs) // gui xong thi nho close channel

	// lấy kết quả từ results, xuất ra màn hình
	for j := 0; j < number; j++ {
		fmt.Println(<-results)
	}

	estimate := time.Since(start)
	logrus.Infof("duration: %v", estimate)
}

func worker(jobs <-chan int, results chan <-int) {
	for n := range jobs {
		results <- fib(n)
	}
}

func fib(n int) int {
	if n <= 1 {
		return n
	}
	return fib(n-1) + fib(n-2)
}
