module standard-pattern

go 1.16

require (
	github.com/caarlos0/env/v6 v6.7.1 // indirect
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	gitlab.com/goxp/cloud0 v1.5.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	gorm.io/gorm v1.21.16 // indirect
)
