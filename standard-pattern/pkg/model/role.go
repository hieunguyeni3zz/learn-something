package model

type (
	Roles struct {
		ID   uint   `json:"id" gorm:"primaryKey, autoIncrement"`
		Name string `json:"name"`
	}

	UserRoles struct {
		UserID uint  `json:"user_id"`
		RoleID uint  `json:"role_id"`
		Roles  Roles `json:"-" gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;foreignKey:role_id"`
	}

	// user_role_request
	UsrRRequest struct {
		UserID     uint   `json:"user_id"`
		Permissions []uint `json:"permission"`
	}
)
