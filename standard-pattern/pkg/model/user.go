package model

type (
	Users struct {
		ID          uint        `json:"id" gorm:"primaryKey,autoIncrement"`
		Email       string      `json:"email"`
		Password    []byte      `json:"-"`
		DisplayName string      `json:"displayName"`
		UserRoles   []UserRoles `json:"-" gorm:"foreignKey:user_id"`
	}

	UserRequest struct {
		Email           string `json:"email"`
		Password        string `json:"password"`
		DisplayName     string `json:"displayName"`
		ConfirmPassword string `json:"confirmPassword"`
	}

	LoginRequest struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	Authorization struct {
		TokenHeader string `header:"Authorization"`
	}
)
