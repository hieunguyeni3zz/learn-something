package repo

import (
	"standard-pattern/pkg/model"

	"gorm.io/gorm"
)

type IRoleRepo interface {
	CheckRole(role string) (*model.Roles, error)
	Create(role *model.Roles) (*model.Roles, error)
}

func NewRoleRepo(pg *gorm.DB) IRoleRepo {
	repo := &Repo{
		Postgres: pg,
	}
	return repo
}

func (h *Repo) CheckRole(name string) (*model.Roles, error) {
	rs := &model.Roles{}

	if err := h.Postgres.Where("name = ?", name).First(rs).Error; err != nil {
		// 2 case
		// case 1:
		// error not found => email not existed
		// case 2:
		// database err -> can not connect to db
		return nil, err
	}
	return rs, nil
}

func (h *Repo) Create(role *model.Roles) (*model.Roles, error) {
	if err := h.Postgres.Create(role).Error; err != nil {
		return nil, err
	}
	return role, nil
}
