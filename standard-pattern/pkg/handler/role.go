package handler

import (
	"net/http"
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/service"

	"gitlab.com/goxp/cloud0/ginext"
)

type RoleHandler struct {
	RoleSrv service.IRoleService
}

func NewRoleHandler(role service.IRoleService) *RoleHandler {
	return &RoleHandler{
		RoleSrv: role,
	}
}

type IRoler interface {
}

func (h *RoleHandler) Create(c *ginext.Request) (*ginext.Response, error) {
	// parse body request, call service to create
	req := model.Roles{}

	c.MustBind(&req)

	rs, err := h.RoleSrv.Create(&req)
	if err != nil {
		return nil, ginext.NewError(http.StatusBadRequest, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, rs), nil
}
