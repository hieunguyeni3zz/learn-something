package utils

import (
	"errors"
	"fmt"

	"gorm.io/gorm"
)

func CheckConfirmPassword(password, confirmPassword string) error {
	if password != confirmPassword {
		return fmt.Errorf("password & confirm password incorrect")
	}
	return nil
}

func IsErrNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}
