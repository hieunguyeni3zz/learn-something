package route

import (
	"standard-pattern/pkg/handler"
	"standard-pattern/pkg/middleware"
	"standard-pattern/pkg/repo"
	srv "standard-pattern/pkg/service"

	"github.com/caarlos0/env/v6"
	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/service"
)

type extraSetting struct {
	DBDebugEnable bool `env:"DB_DEBUG_ENABLE" envDefault:"true"`
}

type Service struct {
	*service.BaseApp
	setting *extraSetting
}

func NewService() *Service {
	s := &Service{
		service.NewApp("register", "v1.0"),
		&extraSetting{},
	}

	_ = env.Parse(s.setting)

	db := s.GetDB()

	if s.setting.DBDebugEnable {
		db = db.Debug()
	}

	newRepo := repo.NewRepo(db)
	newRoleRepo := repo.NewRoleRepo(db)

	userService := srv.NewService(newRepo)
	authService := srv.NewAuthService()
	roleService := srv.NewRoleService(newRoleRepo)

	userHandler := handler.NewHandler(userService)
	authHandler := handler.NewAuthHandler(userService, authService)
	middleHandler := middleware.NewMiddleHandler(authService)
	roleHandler := handler.NewRoleHandler(roleService)

	v1Api := s.Router.Group("/api/v1")
	// auth
	v1Api.POST("auth/login", ginext.WrapHandler(authHandler.Login))

	// user
	v1Api.POST("user/sign-up", ginext.WrapHandler(userHandler.Register))

	// role
	v1Api.POST("role/create", ginext.WrapHandler(roleHandler.Create))

	// middleware
	v1Api.Use(middleHandler.IsAuthenticated())
	{
		v1Api.GET("user", ginext.WrapHandler(userHandler.GetUser))
	}

	// check admin
	v1ApiAdmin := s.Router.Group("/api/v1")
	v1ApiAdmin.Use(middleHandler.CheckAdmin())
	{
		v1ApiAdmin.POST("user/promote", ginext.WrapHandler(userHandler.Promote))
	}

	migrate := handler.NewMigrationHandler(db)
	s.Router.POST("/internal/migrate", migrate.Migrate) // create database - auto migrate

	return s
}
