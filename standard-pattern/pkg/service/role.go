package service

import (
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/repo"
	"standard-pattern/pkg/utils"
)

type RoleService struct{
	Repo repo.IRoleRepo
}

func NewRoleService(repo repo.IRoleRepo) IRoleService {
	return &RoleService{
		Repo: repo,
	}
}

type IRoleService interface {
	Create(role *model.Roles) (*model.Roles, error)
}

func (h *RoleService) Create(rReq *model.Roles) (*model.Roles, error) {
	role, err := h.Repo.CheckRole(rReq.Name)
	if err != nil && !utils.IsErrNotFound(err) {
		return nil, err
	}
	if role != nil {
		return nil, err
	}

	rReq, err = h.Repo.Create(rReq)
	if err != nil {
		return nil, err
	}
	
	return rReq, nil
}
