package service

import (
	"standard-pattern/conf"
	"standard-pattern/pkg/model"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	Email       string   `json:"email"`
	DisplayName string   `json:"displayName"`
	Permissions []string `json:"permissions"`
	jwt.StandardClaims
}

type AuthService struct {
}

func NewAuthService() IAuthService {
	return &AuthService{}
}

type IAuthService interface {
	GenJWTToken(user *model.Users) (string, error)
	ParseJWTToken(token string) (*Claims, error)
}

func (s *AuthService) GenJWTToken(user *model.Users) (string, error) {
	// implement JWT claim
	// TODO: return a JWT that have Email, DisplayName

	permissions := []string{}

	for _, v := range user.UserRoles {
		var s string = strconv.FormatUint(uint64(v.RoleID), 10)
		permissions = append(permissions, s)
	}

	expirationTime := time.Now().Add(120 * time.Second)
	claims := &Claims{
		Email:       user.Email,
		DisplayName: user.DisplayName,
		Permissions: permissions,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(conf.LoadEnv().SecretKey))
}

func (s *AuthService) ParseJWTToken(token string) (*Claims, error) {
	claims := &Claims{}

	jwtToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(conf.LoadEnv().SecretKey), nil
	})

	payload, ok := jwtToken.Claims.(*Claims)
	if !ok {
		return nil, err
	}

	return payload, nil
}
